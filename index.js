'use strict';

const cluster = require('cluster');
const numCPUs = require('os').cpus().length;

const { getTerminalInput } = require('./input');

(async function main () {

  if (cluster.isMaster) {

    const workerData = await getTerminalInput(numCPUs);

    for (let i = 0; i < workerData.length; i++) {

      const worker = cluster.fork();
      const params = { filenames: workerData[i] };

      worker.send(params);

    }

  } else {

    require('./worker');

  }

})();

